/*
This sketch turns on a light when a switch is connected, 
then turns it off after a number of minutes defined by
int timeOutMins with a brief period of half brightness 
as a warning to flip the swith. 

The switch is connected to digital port 6 with a pull 
down resistor, and the light controller is conected to port 5. 
Set bool isReverse true if your light requires a pwm of 0 for 
on and 255 for off, false for a regular configuration.

Authored by Joseph Doyle
This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
Please direct questions or comments to j@random-stuff.org
*/

int timeOutMins = 60;
bool isReverse = true;
int light = 5;
int lightBright;
int button = 6;
bool isButton = false;
bool isLight = false;
unsigned long time;



void setup() {
  pinMode(light, OUTPUT);
  pinMode(button, INPUT);
  Serial.begin(9600);
}

void loop() {
  while(digitalRead(button)){
    if(isReverse){
      time = millis();
      while(millis() < time+1000*60*timeOutMins && digitalRead(button)){
        analogWrite(light,0);
        lightBright = 0;
      }
      while(lightBright < 200 && digitalRead(button)){
        time = millis();
        while(millis() < time + 15 && digitalRead(button)){
          lightBright = lightBright + 1;
          analogWrite(light, lightBright);
          //Serial.println(lightBright);
        }
      }
      time = millis();
      while(millis() < time+1000*5 && digitalRead(button)){
        analogWrite(light,127);
        lightBright = 127;
      }
      while(lightBright < 255 && digitalRead(button)){
        time = millis();
        while(millis() < time + 15 && digitalRead(button)){
          lightBright = lightBright + 1;
          analogWrite(light, lightBright);
          //Serial.println(lightBright);
        }
      }
      analogWrite(light, 255);
      while(digitalRead(button)){
        delay(1);
      }
    }
  else{
    time = millis();
      while(millis() < time+1000*60*timeOutMins && digitalRead(button)){
        analogWrite(light,255);
        lightBright = 255;
      }
      while(lightBright > 100 && digitalRead(button)){
        time = millis();
        while(millis() < time + 15 && digitalRead(button)){
          lightBright = lightBright - 1;
          analogWrite(light, lightBright);
          //Serial.println(lightBright);
        }
      }
      time = millis();
      while(millis() < time+1000*5 && digitalRead(button)){
        analogWrite(light,127);
        lightBright = 127;
      }
      while(lightBright > 0 && digitalRead(button)){
        time = millis();
        while(millis() < time + 15 && digitalRead(button)){
          lightBright = lightBright - 1;
          analogWrite(light, lightBright);
          //Serial.println(lightBright);
        }
      }
      analogWrite(light, 0);
      while(digitalRead(button)){
        delay(1);
      }
    }
  }
}
